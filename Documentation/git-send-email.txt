git-send-email(1)
=================

NAME
----
git-send-email - Send a collection of patches as emails


SYNOPSIS
--------
'git-send-email' [options] <file|directory> [... file|directory]



DESCRIPTION
-----------
Takes the patches given on the command line and emails them out.

The header of the email is configurable by command line options.  If not
specified on the command line, the user will be prompted with a ReadLine
enabled interface to provide the necessary information.

OPTIONS
-------
The options available are:

--bcc::
	Specify a "Bcc:" value for each email.
+
The --bcc option must be repeated for each user you want on the bcc list.

--cc::
	Specify a starting "Cc:" value for each email.
+
The --cc option must be repeated for each user you want on the cc list.

--chain-reply-to, --no-chain-reply-to::
	If this is set, each email will be sent as a reply to the previous
	email sent.  If disabled with "--no-chain-reply-to", all emails after
	the first will be sent as replies to the first email sent.  When using
	this, it is recommended that the first file given be an overview of the
	entire patch series.
	Default is the value of the 'sendemail.chainreplyto' configuration
	value; if that is unspecified, default to --chain-reply-to.

--compose::
	Use $EDITOR to edit an introductory message for the
	patch series.

--from::
	Specify the sender of the emails.  This will default to
	the value GIT_COMMITTER_IDENT, as returned by "git-var -l".
	The user will still be prompted to confirm this entry.

--in-reply-to::
	Specify the contents of the first In-Reply-To header.
	Subsequent emails will refer to the previous email
	instead of this if --chain-reply-to is set (the default)
	Only necessary if --compose is also set.  If --compose
	is not set, this will be prompted for.

--signed-off-by-cc, --no-signed-off-by-cc::
        If this is set, add emails found in Signed-off-by: or Cc: lines to the
        cc list.
        Default is the value of 'sendemail.signedoffbycc' configuration value;
        if that is unspecified, default to --signed-off-by-cc.

--quiet::
	Make git-send-email less verbose.  One line per email should be
	all that is output.

--smtp-server::
	If set, specifies the outgoing SMTP server to use (e.g.
	`smtp.example.com` or a raw IP address).  Alternatively it can
	specify a full pathname of a sendmail-like program instead;
	the program must support the `-i` option.  Default value can
	be specified by the 'sendemail.smtpserver' configuration
	option; the built-in default is `/usr/sbin/sendmail` or
	`/usr/lib/sendmail` if such program is available, or
	`localhost` otherwise.

--subject::
	Specify the initial subject of the email thread.
	Only necessary if --compose is also set.  If --compose
	is not set, this will be prompted for.

--suppress-from, --no-suppress-from::
        If this is set, do not add the From: address to the cc: list, if it
        shows up in a From: line.
        Default is the value of 'sendemail.suppressfrom' configuration value;
        if that is unspecified, default to --no-supress-from.

--thread, --no-thread::
	If this is set, the In-Reply-To header will be set on each email sent.
	If disabled with "--no-thread", no emails will have the In-Reply-To
	header set.
	Default is the value of the 'sendemail.thread' configuration value;
	if that is unspecified, default to --thread.

--dry-run::
	Do everything except actually send the emails.

--envelope-sender::
	Specify the envelope sender used to send the emails.
	This is useful if your default address is not the address that is
	subscribed to a list. If you use the sendmail binary, you must have
	suitable privileges for the -f parameter.

--to::
	Specify the primary recipient of the emails generated.
	Generally, this will be the upstream maintainer of the
	project involved.
+
The --to option must be repeated for each user you want on the to list.


CONFIGURATION
-------------
sendemail.aliasesfile::
	To avoid typing long email addresses, point this to one or more
	email aliases files.  You must also supply 'sendemail.aliasfiletype'.

sendemail.aliasfiletype::
	Format of the file(s) specified in sendemail.aliasesfile. Must be
	one of 'mutt', 'mailrc', 'pine', or 'gnus'.

sendemail.bcc::
	Email address (or alias) to always bcc.

sendemail.chainreplyto::
	Boolean value specifying the default to the '--chain_reply_to'
	parameter.

sendemail.smtpserver::
	Default smtp server to use.

Author
------
Written by Ryan Anderson <ryan@michonline.com>

git-send-email is originally based upon
send_lots_of_email.pl by Greg Kroah-Hartman.

Documentation
--------------
Documentation by Ryan Anderson

GIT
---
Part of the gitlink:git[7] suite
